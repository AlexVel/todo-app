import React from 'react';
import { IUseLocalStorageResponse } from '../interfaces/use-local-storage';



export function useLocalStorage<T>(key: string, initialValue: T): IUseLocalStorageResponse<T> {
  let storage;
  try {
    storage = JSON.parse(localStorage.getItem(key) as string);
  } catch(e) {}

  const [data, setData] = React.useState(storage || initialValue);

  function setDataToStorage (data: T) {
    setData(data);
    localStorage.setItem(key, JSON.stringify(data));
  }

  return [data, setDataToStorage];
}
