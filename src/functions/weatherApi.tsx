import IWeather  from "../interfaces/iDataFromSite";

const myApiKey: string = 'd77ff78d9ac25918052a7e88299e703c'; 


const getWeather = async(city: string, key = myApiKey): Promise<IWeather|null>  => {

  const api_url = await fetch(`https://api.openweathermap.org/data/2.5/weather?q=${city}&appid=${key}`);
  let data = await api_url.json()
  
  if(data.cod >= 400 && data.cod <= 499) {
    return null;
  }

  let dataObj:IWeather  = {name:data.name , weather:data.weather[0].description, temp:data.main.temp, key:data.id}
  return dataObj
}



export default getWeather;