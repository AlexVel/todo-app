import './style.css'

interface IEndGameMessage {
  player: string
  reset: ()=> void;
  draw: boolean;
}

export function EndGameMessage ({player, reset, draw}: IEndGameMessage) {

  return (
    <div className="overlay">
      <div className="WinWrapper">
        <div className="text_box">
          {draw ? "Draw" : `Player ${player ==="o"? "X": "O"} Win`}
        </div>
        <button className="playAgainBtn" onClick={reset}>play again</button>
      </div>      
    </div>
  )
}