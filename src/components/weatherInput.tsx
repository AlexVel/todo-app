import React from 'react';

interface InputWeatherProp {
  funcAddCity: (city: string) => void;
}

const InputWeather = ({funcAddCity}: InputWeatherProp) => {

  function add() {
    let infoFromInput= document.querySelector('.inputWeather') as HTMLInputElement;
    let city: string = infoFromInput.value
    if (!city||city === " ") return;
    infoFromInput.value = "";
    let cityLC = city.toLowerCase()
    funcAddCity(cityLC)
  }

  return (
    <div className="inputWeatherWrap">
      <input placeholder="City" className="inputWeather"></input>
      <button  onClick={()=>add()}>Add</button>
    </div>
  )
}

export default InputWeather;