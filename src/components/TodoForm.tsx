import React, { useState }  from 'react';
import DatePicker from "react-datepicker";
import 'react-datepicker/dist/react-datepicker.css';

interface TodoFormProp {
  addTodo: (text: string, date: number) => void;
}

export function TodoForm({ addTodo }: TodoFormProp) {
  const [value, setValue] = useState<string>("");
  const [startDate, setStartDate] = useState<Date>();
  const [timeStamp, setTimeStamp] = useState(Number);

  const handleSubmit = (e: React.FormEvent<HTMLFormElement>) => {

    e.preventDefault();
    if (!value) return;
    addTodo(value, timeStamp);
    setValue("");
    setStartDate(undefined);
    setTimeStamp(0);
  };

  return (
    <form onSubmit={handleSubmit} > 
      
      <input
        type="text"
        className="input"
        value={value}
        onChange={e => {
          setValue(e.target.value)
        }}
      /> 
      
      <DatePicker 
        placeholderText="pick date"
        className = "dataPicker"
        dateFormat="dd/MM/yyyy"
        selected={startDate} 
        onChange={(date: Date) => {
          date.setHours(23, 59, 59, 999)
          setTimeStamp(+date);
          setStartDate(date)
          }
        }
      />

      <div>
        <button>add</button>
      </div>
      
    </form>
  );
}

