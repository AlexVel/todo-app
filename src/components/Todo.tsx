
import Itodo from '../interfaces/Itodo'

interface TodoProps {
  todo: Itodo;
  changeState:(i:number) => void;
  removeTodo: (i:number) => void;
}

export function Todo({ todo, changeState, removeTodo}: TodoProps) {

  let unixDate: number  = todo.date!
  let stringDate = new Date(unixDate).toDateString()

  return (
    <div className = "todo">

      <div className={todo.isCompleted ? " strike" : todo.date! < Date.now() &&  todo.date! !==0 ? "todoIsExpired": ""}> 
        {todo.text}
      </div>
    
      <div> 
        <span className="dateHolder">
          {unixDate !==0 ? stringDate :""}
        </span>
        <button onClick={() => changeState(todo.id)}> {todo.isCompleted ? " unfinished" : "complete"} </button> 
        
        <button onClick={ () => removeTodo(todo.id) }>x</button>
      </div>   
    </div>
  )
}

