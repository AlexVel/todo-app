import React, { useState } from 'react';
import '../headStyle.css';
import {
  Link,
} from "react-router-dom";


export function HeaderMobile() {

  const [stateBtn, setStateBtn] = useState<string>('toClose')

  const btnMenu = () => {
    stateBtn === "toClose" ?  setStateBtn('toOpen'): setStateBtn('toClose');
  }

  if (stateBtn === "toOpen" ) {
    return (
      <div className="mobileMenu">
        <button onClick ={()=>btnMenu()}>
           menu <img src="./white-menu-icon-4.jpg" alt=""/> 
        </button>
        <ul>
          <li><Link to="/">Home</Link></li>
          <li><Link to="/weather">Weather</Link></li>
          <li><Link to="/tic-tac-toe">TicTacToe</Link></li>
        </ul>
      </div>
    ) 
  } else {
    return (
      <div className="mobileMenu">

        <button onClick ={()=>btnMenu()}>
          menu <img src="./white-menu-icon-4.jpg" alt=""/> 
        </button>

      </div>
    )
  }
}

