import IWeather from '../interfaces/iDataFromSite'


interface IOneCityWrapProps {
  obj: IWeather;
  del: (i:number) => void;
  index: number;
}


function OneCityWeather({obj, del, index}: IOneCityWrapProps  ) {
  return (
    <div className="oneCityWeather">
      <div className="cityName">
        <span className="city">{obj.name}</span>
        <button className="delCity" onClick={() =>del(index)} >X</button>
      </div>

      <div className="infWrapper">
        <div className="inf temperature">presently</div>
        <div className="inf weather">{obj.weather}</div>    
        <div className="inf">{obj.temp}°C</div>                
      </div>
    </div>
  )
}

export default OneCityWeather