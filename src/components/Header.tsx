import React from 'react';

import {
  Link,
} from "react-router-dom";
import { HeaderMobile } from './mobileMenuComponent';


 export function Header() {
  return (
    <header className="header">

      <div className="headerContainer">
        <div className="logo">ReactToDo</div>
        <span className="links">
          
          <span className="headspan">
            <Link to="/">Home</Link>
          </span>

          <span className="headspan">
            <Link to="/weather">Weather</Link>
          </span>

          <span className="headspan">
            <Link to="/tic-tac-toe">TicTacToe</Link>
          </span>   

        </span>
        <span className="headersIconsRight"></span>
      </div>
      <HeaderMobile />

    </header>
  )
}


export default Header