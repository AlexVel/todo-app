import './style.css'

interface ISwitcher {
  player: string
}


export function Switcher({player}: ISwitcher) {

  return (
    <div className="definePlayer">
    x
    <div className="switcherWrapper">
      
      <div className="switcherLine">
        <div className={player ==='x' ? "switcherPoint" : "switcherPoint switchToRight"}></div>
      </div>

    </div>
    о
  </div>
  )
}