export default interface Itodo {
  text: string;
  date?: number;
  isCompleted: boolean;
  id: number;
}