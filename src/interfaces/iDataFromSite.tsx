export default interface IWeather {
  name: string;
  weather: string;
  temp: number;
  key?: number;
  wind?: any;
}