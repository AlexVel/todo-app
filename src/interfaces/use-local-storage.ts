export type IUseLocalStorageResponse<T> = [
  T,
  (data: T) => void
]