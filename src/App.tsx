import React from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
} from "react-router-dom";


import './App.css';
import Weather from './pages/Weather';
import ToDoPage from './pages/ToDoPage';
import Header from './components/Header';

import { Footer } from './components/Footer';
import TicTacToe from './pages/TicTacToe/TicTacToe';

export default function App() {
  return (
    <div>
      
      <Router>
        <div className="contentWithoutFoot">
          <Header />
            <div>      

              <Switch>
                <Route path="/weather">
                  <Weather />
                </Route>
                
                <Route path="/tic-tac-toe">
                  <TicTacToe />
                </Route>

                <Route path="/">
                  <ToDoPage />
                </Route>
                
              </Switch>
            </div>
        </div> 
      </Router>        
      <Footer />
    </div>
    
  );
}