import React, { useState } from 'react';
import { TodoForm } from '../components/TodoForm';
import { useLocalStorage } from '../hooks/local-storage';
import { Todo } from '../components/Todo';
import Itodo from '../interfaces/Itodo'


function ToDoPage() {
  
  const [filter, setFilter] = useState('all')

  const [todos, setTodos] = useLocalStorage<Itodo[]>('todo', [
    { 
      text: "Learn about React",
      date: 1,
      isCompleted: false,
      id: Date.now(),
    },
    { 
      text: "Meet friend for lunch", 
      date: 1,
      isCompleted: false,
      id: Date.now()-1,
    },
    { 
      text: "Build really cool todo app",
      date: 1,
      isCompleted: false,
      id: Date.now()-2,
    }
  ]);

  const addTodo = (text: string, date: number, isCompleted: boolean = false) => {
    let id: number = Date.now();
    const newTodos = [...todos, { text, date , isCompleted, id }];
    setTodos(newTodos);
  };

  const changeState = (id: number) => {
    const newTodos = todos.map(todo => {
      if(todo.id === id) todo.isCompleted = !todo.isCompleted; 
      return todo
      }
    );
    setTodos(newTodos) 
  }
  
  const removeTodo = (id: number) => {
    let newTodos = todos.filter(todo => 
      (todo.id !== id)
    )
    setTodos(newTodos);
  };

  const filterFunction = (todo: Itodo) => {
    if (filter === "expired") {
       return  todo.date! < (Date.now()) &&  todo.date !==0
    } else if (filter === "completed") {
      return !todo.isCompleted
    } else if (filter === "unfinished") {
      return todo.isCompleted
    } else {
      return true
    }          
  }

  return (
    <div className="todo-list">

      <select onChange={ e => {setFilter(e.target.value)}}>
        <option value="all">all</option>
        <option value="completed">completed</option>
        <option value="unfinished">unfinished</option>
        <option value="expired">expired</option>
      </select>

      {todos
       .filter(filterFunction)
       .map((todo) => (
            
        <Todo
          key={todo.id}
          todo={todo}
          changeState={changeState}
          removeTodo={removeTodo}
        />
          
       ))}
      <TodoForm addTodo={addTodo} />
    </div>
  );
}

export default ToDoPage;

