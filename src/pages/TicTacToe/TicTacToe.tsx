import React, { useState } from 'react';
import { Switcher } from '../../components/Switcher/Switcher';
import { EndGameMessage } from '../../components/TicTacToeEndMessage/EndGameMessage';
import './style.css'

function TicTacToe() {

  const [playState, setPlayState] = useState([
    '','','',
    '','','',
    '','','',
  ] )

  const[activePlayer, setPlayer]  = useState('x')

  const[endGame, setEndGame] = useState({
    theEnd: false,
    draw: false
  })

  const setMove =(index: number) => {
    
    if(!playState[index]){
  
      let newPlayState = playState
      newPlayState[index] = activePlayer; 
      setPlayState(newPlayState)
      checkWinner(playState)
      setPlayer(activePlayer === "x" ? "o": "x")
    }
  }

  const checkWinner = (playState : string[]) => {

    const varies =  [
      [0, 1, 2],
      [3, 4, 5], 
      [6, 7, 8],

      [0, 3, 6], 
      [1, 4, 7], 
      [2, 5, 8], 
      
      [0, 4, 8], 
      [2, 4, 6],
    ]; 

    for(let vary of varies) {
      if (
        playState[vary[0]] 
        && playState[vary[0]] === playState[vary[1]]
        && playState[vary[1]] === playState[vary[2]]
      ) {
        setEndGame({theEnd:true, draw: false})
        return
      }
    }

    if(!playState.includes('')) setEndGame({theEnd: true, draw: true});
  };

  const resetGame =()=> {
    setPlayState([
      '','','',
      '','','',
      '','','',
    ])
    setEndGame({
      theEnd: false,
      draw: false
    })
    setPlayer('x')
  }

  return (
    <div className="TTTPageWrapper">
      

      <Switcher
        player = {activePlayer}
      />

      <div className="tic-tac-toe_fieldWrapper" >
        {
          playState.map( (cell, index: number) => (
            <div
              key={index}
              className="cell"
              onClick={() => setMove(index)}>
                  
                {cell}
            </div>
          ))
        }
      </div> 
      

      { endGame.theEnd ? 
        <EndGameMessage
          player ={activePlayer}
          reset = {resetGame}
          draw ={endGame.draw}
        />           
        : null
      }        

    </div>
  );
}

export default TicTacToe;