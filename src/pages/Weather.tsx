import React, { useEffect, useState } from 'react';
import OneCityWeather from '../components/OneCityWeatherCard'; 
import InputWeather from '../components/weatherInput';
import getWeather from '../functions/weatherApi';
import { useLocalStorage } from '../hooks/local-storage';
import IWeather from '../interfaces/iDataFromSite';




function Weather() {
  const [citiesList, setCitiesList] = useLocalStorage<string[]>('citiesList',[
    "Kyiv",
    "moscow",
    "london",
  ])

  const [allCitiesData, setAllCitiesData] = useState<IWeather[]>([])
  
  async function getWeatherList(list:string[]): Promise<void> {
    let response = await Promise.all(
      list.map(element => (getWeather(element)))
    )
    setAllCitiesData(response.filter(data => !!data) as IWeather[])
  }
  
  useEffect(() => {
    getWeatherList(citiesList)
  }, [citiesList])

  const addCity = (city:string) => {

    if (citiesList.includes(city)){
      alert('there is already this city on the page')
      return
    }
    
    getWeather(city).then(data => {
      if(data) {
        const newCitiesList = [...citiesList, city];
        setCitiesList(newCitiesList);
      } else {
        alert('city is not found')
      }
    })
  };

  const deleteCity =(index: number) => {
    const newCitiesList = [...citiesList];
    newCitiesList.splice(index, 1);
    setCitiesList(newCitiesList);
  }
  
  return (
  <div className="weatherWrapper">

   <InputWeather 
      funcAddCity = {addCity}
   />

    <div className="CitiesWrapper">
      {allCitiesData.map((city, index) => (
        <OneCityWeather 
        index={index}
        key={index}
        obj={city}
        del = {deleteCity}
        />       
      ))}
    </div>
  </div>);
}




export default Weather;